# Toolchains

A collection of Dockerfiles defining toolchains used to build freeflight and
related software.

## Project Structure

Each subdirectory contains a Dockerfile and other required metadata necessary to
build a container image with a cross toolchain installed. The directory name
describes the compilation target for the cross toolchain and each cross toolchain
is compiled to run on an x86_64 build host.
