# Raspberry Pi 4 CMake toolchain file

set(TARGET_TRIPLET aarch64-rpi4-linux-gnu)

set(CMAKE_SYSTEM_NAME Linux)
set(CMAKE_SYSTEM_PROCESSOR aarch64)
set(CMAKE_C_COMPILER "/usr/local/bin/${TARGET_TRIPLET}-gcc")
set(CMAKE_CXX_COMPILER "/usr/local/bin/${TARGET_TRIPLET}-g++")
set(CMAKE_SYSROOT "/usr/local/${TARGET_TRIPLET}/sysroot")

# Search libraries only under *target* paths.
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)

unset(TARGET_TRIPLET)
